User Environment
----------------

APT_CONFIG=/var/lib/sbuild/apt.conf
DEB_BUILD_OPTIONS=parallel=4
HOME=/sbuild-nonexistent
LANG=C.UTF-8
LC_ALL=C.UTF-8
LOGNAME=buildd
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games
SCHROOT_ALIAS_NAME=build-PACKAGEBUILD-20128255
SCHROOT_CHROOT_NAME=build-PACKAGEBUILD-20128255
SCHROOT_COMMAND=env
SCHROOT_GID=2501
SCHROOT_GROUP=buildd
SCHROOT_SESSION_ID=build-PACKAGEBUILD-20128255
SCHROOT_UID=2001
SCHROOT_USER=buildd
SHELL=/bin/sh
TERM=unknown
USER=buildd
V=1

dpkg-buildpackage
-----------------

dpkg-buildpackage: info: source package xorg
dpkg-buildpackage: info: source version 1:7.7+19ubuntu15
dpkg-buildpackage: info: source distribution groovy
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 fakeroot debian/rules clean
dh clean 
   debian/rules override_dh_auto_clean
make[1]: Entering directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
/usr/bin/make -C xsf-docs clean
make[2]: Entering directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15/xsf-docs'
Removing all generated files
rm -f ./howto/configure-input.html ./howto/triage-bugs.html ./howto/report-bugs.html ./howto/build-mesa.html ./howto/use-gdb.html ./howto/use-xrandr.html ./reference/squeeze-backports.html ./reference/dependencies.html ./reference/experimental.html ./reference/git-usage.html ./reference/upstream-contacts.html ./upstream-features.html ./index.html ./faq/general.html ./howto/configure-input.pdf ./howto/triage-bugs.pdf ./howto/report-bugs.pdf ./howto/build-mesa.pdf ./howto/use-gdb.pdf ./howto/use-xrandr.pdf ./reference/squeeze-backports.pdf ./reference/dependencies.pdf ./reference/experimental.pdf ./reference/git-usage.pdf ./reference/upstream-contacts.pdf ./upstream-features.pdf ./index.pdf ./faq/general.pdf
make[2]: Leaving directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15/xsf-docs'
make[1]: Leaving directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
   dh_clean
 debian/rules build
dh build 
   dh_update_autotools_config
   dh_autoreconf
   debian/rules override_dh_auto_build
make[1]: Entering directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
/usr/bin/make -C xsf-docs
make[2]: Entering directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15/xsf-docs'
 HTML howto/configure-input.html
 HOME howto/configure-input.html
 CSS  howto/configure-input.html
 JS   howto/configure-input.html
 HTML howto/triage-bugs.html
 HOME howto/triage-bugs.html
 CSS  howto/triage-bugs.html
 JS   howto/triage-bugs.html
 HTML howto/report-bugs.html
 HOME howto/report-bugs.html
 CSS  howto/report-bugs.html
 JS   howto/report-bugs.html
 HTML howto/build-mesa.html
 HOME howto/build-mesa.html
 CSS  howto/build-mesa.html
 JS   howto/build-mesa.html
 HTML howto/use-gdb.html
 HOME howto/use-gdb.html
 CSS  howto/use-gdb.html
 JS   howto/use-gdb.html
 HTML howto/use-xrandr.html
 HOME howto/use-xrandr.html
 CSS  howto/use-xrandr.html
 JS   howto/use-xrandr.html
 HTML reference/squeeze-backports.html
 HOME reference/squeeze-backports.html
 CSS  reference/squeeze-backports.html
 JS   reference/squeeze-backports.html
 HTML reference/dependencies.html
 HOME reference/dependencies.html
 CSS  reference/dependencies.html
 JS   reference/dependencies.html
 HTML reference/experimental.html
 HOME reference/experimental.html
 CSS  reference/experimental.html
 JS   reference/experimental.html
 HTML reference/git-usage.html
 HOME reference/git-usage.html
 CSS  reference/git-usage.html
 JS   reference/git-usage.html
 HTML reference/upstream-contacts.html
 HOME reference/upstream-contacts.html
 CSS  reference/upstream-contacts.html
 JS   reference/upstream-contacts.html
 HTML upstream-features.html
 HOME upstream-features.html
 CSS  upstream-features.html
 JS   upstream-features.html
 HTML index.html
 HOME index.html
 CSS  index.html
 JS   index.html
 HTML faq/general.html
 HOME faq/general.html
 CSS  faq/general.html
 JS   faq/general.html
make[2]: Leaving directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15/xsf-docs'
make[1]: Leaving directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
   create-stamp debian/debhelper-build-stamp
 fakeroot debian/rules binary
dh binary 
   dh_testroot
   dh_prep
   dh_installdirs
   debian/rules override_dh_install
make[1]: Entering directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
/usr/bin/make -C xsf-docs install DESTDIR=/<<BUILDDIR>>/xorg-7.7+19ubuntu15/debian/xserver-xorg/usr/share/doc/xorg
make[2]: Entering directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15/xsf-docs'
mkdir -p /<<BUILDDIR>>/xorg-7.7+19ubuntu15/debian/xserver-xorg/usr/share/doc/xorg
# There are probably better ways:
set -e; for i in ./howto/configure-input.html ./howto/triage-bugs.html ./howto/report-bugs.html ./howto/build-mesa.html ./howto/use-gdb.html ./howto/use-xrandr.html ./reference/squeeze-backports.html ./reference/dependencies.html ./reference/experimental.html ./reference/git-usage.html ./reference/upstream-contacts.html ./upstream-features.html ./index.html ./faq/general.html ./howto/configure-input.txt ./howto/triage-bugs.txt ./howto/report-bugs.txt ./howto/build-mesa.txt ./howto/use-gdb.txt ./howto/use-xrandr.txt ./reference/squeeze-backports.txt ./reference/dependencies.txt ./reference/experimental.txt ./reference/git-usage.txt ./reference/upstream-contacts.txt ./upstream-features.txt ./index.txt ./faq/general.txt xsf.css asciidoc-xhtml11.css asciidoc-xhtml11.js xsf.svg xsf.png; do \
	d=/<<BUILDDIR>>/xorg-7.7+19ubuntu15/debian/xserver-xorg/usr/share/doc/xorg/`dirname $i` && \
	mkdir -p $d && \
	install $i $d; \
done
make[2]: Leaving directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15/xsf-docs'
dh_install
make[1]: Leaving directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
   dh_installdocs
   dh_installchangelogs
   dh_installman
   debian/rules override_dh_installinit-indep
make[1]: Entering directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
dh_installinit -px11-common -u'start 70 S .'
make[1]: Leaving directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
   dh_lintian
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   debian/rules override_dh_fixperms-indep
make[1]: Entering directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
dh_fixperms
chown root:root debian/x11-common/etc/X11/Xreset
chmod 755 debian/x11-common/etc/X11/Xreset
chown root:root debian/x11-common/etc/X11/Xsession
chmod 755 debian/x11-common/etc/X11/Xsession
make[1]: Leaving directory '/<<BUILDDIR>>/xorg-7.7+19ubuntu15'
   dh_fixperms -Nx11-common -Nxorg-dev -Nxbase-clients -Nxutils
   dh_missing
   dh_strip
   dh_makeshlibs
   dh_shlibdeps
   dh_installdeb
   dh_gencontrol
dpkg-gencontrol: warning: Depends field of package xserver-xorg: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums
   dh_builddeb
INFO: pkgstriptranslations version 146
INFO: pkgstriptranslations version 146
INFO: pkgstriptranslations version 146
INFO: pkgstriptranslations version 146
pkgstriptranslations: processing xbase-clients (in debian/xbase-clients); do_strip: 1, oemstrip: 
pkgstriptranslations: processing xserver-xorg-video-all (in debian/xserver-xorg-video-all); do_strip: 1, oemstrip: 
pkgstriptranslations: processing xorg (in debian/xorg); do_strip: 1, oemstrip: 
pkgstriptranslations: processing x11-common (in debian/x11-common); do_strip: 1, oemstrip: 
pkgstriptranslations: xserver-xorg-video-all does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgstriptranslations: x11-common does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/xserver-xorg-video-all/DEBIAN/control, package xserver-xorg-video-all, directory debian/xserver-xorg-video-all
INFO: pkgstripfiles: waiting for lock (xserver-xorg-video-all) ...
pkgstripfiles: processing control file: debian/x11-common/DEBIAN/control, package x11-common, directory debian/x11-common
.. removing usr/share/doc/x11-common/changelog.Debian.old.gz
pkgstripfiles: Truncating usr/share/doc/x11-common/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package x11-common ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'x11-common' in '../x11-common_7.7+19ubuntu15_all.deb'.
INFO: pkgstriptranslations version 146
pkgstriptranslations: processing xserver-xorg (in debian/xserver-xorg); do_strip: 1, oemstrip: 
pkgstriptranslations: xserver-xorg does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
INFO: pkgstripfiles: waiting for lock (xserver-xorg-video-all) ...
pkgstripfiles: processing control file: debian/xserver-xorg/DEBIAN/control, package xserver-xorg, directory debian/xserver-xorg
Searching for duplicated docs in dependency xserver-xorg-input-all...
  symlinking changelog.gz in xserver-xorg to file in xserver-xorg-input-all
pkgstripfiles: Running PNG optimization (using 4 cpus) for package xserver-xorg ...
INFO: pkgstripfiles: waiting for lock (xserver-xorg-video-all) ...
o
pkgstripfiles: PNG optimization (1/0) for package xserver-xorg took 1 s
dpkg-deb: building package 'xserver-xorg' in '../xserver-xorg_7.7+19ubuntu15_amd64.deb'.
pkgstripfiles: Truncating usr/share/doc/xserver-xorg-video-all/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package xserver-xorg-video-all ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'xserver-xorg-video-all' in '../xserver-xorg-video-all_7.7+19ubuntu15_amd64.deb'.
INFO: pkgstriptranslations version 146
pkgstriptranslations: processing xserver-xorg-input-all (in debian/xserver-xorg-input-all); do_strip: 1, oemstrip: 
pkgstriptranslations: xserver-xorg-input-all does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/xserver-xorg-input-all/DEBIAN/control, package xserver-xorg-input-all, directory debian/xserver-xorg-input-all
pkgstripfiles: Truncating usr/share/doc/xserver-xorg-input-all/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package xserver-xorg-input-all ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'xserver-xorg-input-all' in '../xserver-xorg-input-all_7.7+19ubuntu15_amd64.deb'.
pkgstriptranslations: xbase-clients does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/xbase-clients/DEBIAN/control, package xbase-clients, directory debian/xbase-clients
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
pkgstriptranslations: xorg does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/xorg/DEBIAN/control, package xorg, directory debian/xorg
Searching for duplicated docs in dependency xserver-xorg...
  symlinking changelog.gz in xorg to file in xserver-xorg-input-all
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
pkgstripfiles: Running PNG optimization (using 4 cpus) for package xorg ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'xorg' in '../xorg_7.7+19ubuntu15_amd64.deb'.
INFO: pkgstriptranslations version 146
pkgstriptranslations: processing xorg-dev (in debian/xorg-dev); do_strip: 1, oemstrip: 
pkgstriptranslations: xorg-dev does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/xorg-dev/DEBIAN/control, package xorg-dev, directory debian/xorg-dev
INFO: pkgstripfiles: waiting for lock (xbase-clients) ...
pkgstripfiles: Truncating usr/share/doc/xorg-dev/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package xorg-dev ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'xorg-dev' in '../xorg-dev_7.7+19ubuntu15_all.deb'.
pkgstripfiles: Truncating usr/share/doc/xbase-clients/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package xbase-clients ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'xbase-clients' in '../xbase-clients_7.7+19ubuntu15_all.deb'.
INFO: pkgstriptranslations version 146
pkgstriptranslations: processing xutils (in debian/xutils); do_strip: 1, oemstrip: 
pkgstriptranslations: xutils does not contain translations, skipping
pkgstriptranslations: no translation files, not creating tarball
pkgmaintainermangler: Not overriding Maintainer for domain lists.ubuntu.com
pkgstripfiles: processing control file: debian/xutils/DEBIAN/control, package xutils, directory debian/xutils
pkgstripfiles: Truncating usr/share/doc/xutils/changelog.gz to topmost ten records
pkgstripfiles: Running PNG optimization (using 4 cpus) for package xutils ...
pkgstripfiles: No PNG files.
dpkg-deb: building package 'xutils' in '../xutils_7.7+19ubuntu15_all.deb'.
 dpkg-genbuildinfo --build=binary
 dpkg-genchanges --build=binary -mLaunchpad Build Daemon <buildd@lcy01-amd64-003.buildd> >../xorg_7.7+19ubuntu15_amd64.changes
dpkg-genchanges: info: binary-only upload (no source code included)
 dpkg-source --after-build .
